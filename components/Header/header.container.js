import React from 'react';
import { View, Text, StyleSheet, StatusBar } from 'react-native';
import { styles } from './header.styles'
import { CHANNEL } from '../../constants'

class Header extends React.Component {
    render() {
        return (
            <View style={styles.header}>
                <StatusBar backgroundColor="#ff7f50" barStyle="light-content" />
                <Text style={styles.title}>
                    @{CHANNEL}
                </Text>
            </View>
        )
    }
}

export default Header