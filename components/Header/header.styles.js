import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    header: {
        height: 60,
        backgroundColor: '#ff7f50',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: 10,
    },
    title: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
    },
});