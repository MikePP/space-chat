import React from 'react';
import { FlatList, View, Image, Text } from 'react-native';
import { styles } from './list.styles'

import { connect } from 'react-redux'
import { getMessagesFromDb } from './list.actions'

class List extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.getMessagesFromDb();
    }

    renderItem({ item }) {
        return (
            <View style={styles.row}>
                <Image style={styles.avatar} source={{ uri: item.avatar }} />
                <View style={styles.rowText}>
                    <Text style={styles.sender}>{item.sender}</Text>
                    <Text style={styles.message}>{item.message}</Text>
                </View>
            </View>
        );
    }

    render() {
        return (
            <FlatList data={this.props.appData.list.filteredMessages} renderItem={this.renderItem}
                ref={ref => this.flatListRef = ref}
                onContentSizeChange={(contentWidth, contentHeight) => {
                    this.flatListRef.scrollToEnd({ animated: true });
                }}
            />
        )
    }
}

function mapStateToProps(state) {
    return {
        appData: state
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getMessagesFromDb: () => dispatch(getMessagesFromDb())
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(List)