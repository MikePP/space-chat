import { subscribeAsync } from './../../Firebase/Api';
import { FETCH_MESSAGES, CHANNEL } from '../../constants'

export function getMessagesFromDb() {
    return {
        type: FETCH_MESSAGES,
        payload: subscribeAsync(CHANNEL)

    }
}