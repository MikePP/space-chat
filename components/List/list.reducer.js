import {
    FETCH_MESSAGES_PENDING,
    FETCH_MESSAGES_FULFILLED,
    FETCH_MESSAGES_REJECTED,
    FETCH_MESSAGES
} from '../../constants'

const initialState = {
    filteredMessages: [],
    messages: [],
    isFetching: false,
    isError: false
}

export default function listReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_MESSAGES_PENDING:
            return {
                ...state,
                isFetching: true
            }
        case FETCH_MESSAGES_FULFILLED:
            return {
                ...state,
                filteredMessages: action.payload
            }

        case FETCH_MESSAGES_REJECTED:
            return {
                ...state,
                isError: true
            }
        default:
            return state
    }
}