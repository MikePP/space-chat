import React from 'react';
import {
    View, Text, KeyboardAvoidingView,
    TextInput, TouchableOpacity
} from 'react-native';
import { styles } from './input.styles'

import { connect } from 'react-redux'
import { setCurrentMessage, saveMessage } from './input.actions'
import { getMessagesFromDb } from './../../components/List/list.actions'

class Input extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <KeyboardAvoidingView behavior="padding">
                <View style={styles.controls}>
                    <TextInput
                        value={this.props.appData.input.currentMessage}
                        onChangeText={text => this.props.setCurrentMessage(text)}
                        style={styles.input}
                        underlineColorAndroid="transparent"
                        placeholder="Type a nice message" />
                    <TouchableOpacity onPress={() => {
                        this.props.saveMessage();
                        this.props.getMessagesFromDb()
                        }}>
                        <Text style={styles.send}>Send</Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingView>
        )
    }
}

//  "state" here isn't just the local state of the object, 
//   but the single state of the redux application
//  "appData" is a object of the local state of this component
function mapStateToProps(state) { 
    return {
        appData: state
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setCurrentMessage: (text) => dispatch(setCurrentMessage(text)),
        saveMessage: () => dispatch(saveMessage()),
        getMessagesFromDb: () => dispatch(getMessagesFromDb()) // best practice would to create a separate action & reducer for this
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Input)