import { save } from './../../Firebase/Api';
import {
    AVATAR, CHANNEL, SENDER,
    SET_CURRENT_MESSAGE, SAVE_MESSAGE
} from '../../constants'

const initialState = {
    currentMessage: ''
}

export default function inputReducer(state = initialState, action) {
    switch (action.type) {
        case SET_CURRENT_MESSAGE:
            return {
                ...state,
                currentMessage: action.currentMessage
            }

        case SAVE_MESSAGE:
            {
                return {
                    ...state,
                    saveMessage: save({
                        avatar: AVATAR,
                        channel: CHANNEL,
                        sender: SENDER,
                        message: state.currentMessage
                    }),
                    currentMessage: ''
                }
            }
        default:
            return state
    }
}