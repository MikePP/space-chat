import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    controls: {
        flexDirection: 'row',
        backgroundColor: '#f2e5d2',
    },
    input: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        fontSize: 18,
        flex: 1,
    },
    send: {
        alignSelf: 'center',
        color: '#ff7f50',
        fontSize: 16,
        fontWeight: 'bold',
        padding: 20,
    }
});