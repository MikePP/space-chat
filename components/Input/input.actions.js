import {
  SET_CURRENT_MESSAGE, SAVE_MESSAGE
} from '../../constants'

export function setCurrentMessage(currentMessage) {
  return {
    type: SET_CURRENT_MESSAGE,
    currentMessage
  }
}

export function saveMessage() {
  return {
    type: SAVE_MESSAGE
  }
}