export const AVATAR = '[Add url to your image]'
export const CHANNEL = 'AlphaCentauri'
export const SENDER = '[Fill your name]'

// for inputReducer
export const SET_CURRENT_MESSAGE = 'SET_CURRENT_MESSAGE'
export const SAVE_MESSAGE = 'SAVE_MESSAGE'

// for listReducer
export const FETCH_MESSAGES = 'FETCH_MESSAGES'
export const FETCH_MESSAGES_PENDING = 'FETCH_MESSAGES_PENDING'
export const FETCH_MESSAGES_FULFILLED = 'FETCH_MESSAGES_FULFILLED'
export const FETCH_MESSAGES_REJECTED = 'FETCH_MESSAGES_REJECTED'