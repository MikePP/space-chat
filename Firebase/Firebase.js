import * as firebase from 'firebase';

// please switch to own config after the course
const config = {
    apiKey: "AIzaSyDCjLhDGCmcIKYPosBNLQ_w_IDy-oGTVMk",
    authDomain: "utu-react.firebaseapp.com",
    databaseURL: "https://utu-react.firebaseio.com",
    projectId: "utu-react",
    storageBucket: "utu-react.appspot.com",
    messagingSenderId: "237740426568"
};

firebase.initializeApp(config);

export default firebase;