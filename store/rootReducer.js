import { combineReducers } from 'redux'
import input from './../components/Input/input.reducer';
import list from './../components/List/list.reducer';

const rootReducer = combineReducers({
    input,
    list
})

export default rootReducer