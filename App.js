import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Provider } from 'react-redux'
import configureStore from './store/configureStore'
import Input from './components/Input/input.container'
import List from './components/List/list.container'
import Header from './components/Header/header.container'

const store = configureStore();

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <Header />
          <List />
          <Input />
        </View>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  }
})